#undef UNICODE

#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"
#include <allegro5/keyboard.h>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <fcntl.h>
#include <winsock2.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>


#pragma comment(lib,"ws2_32.lib") //Winsock Library


#define DEBUG false
#define BUFLEN 512  //Max length of buffer
//#define PORT 8888   //The port on which to listen for incoming data

using namespace std;

const float FPS = 60;
const float data_rate = 0.1;
const float default_timeout = 5.0;
const int SCREEN_W = 800;
const int SCREEN_H = 600;
const int BOUNCER_SIZE = 32;
const float PI = 3.1415;
const int max_clients = 16;
const int max_guns = 5;
const float SCALE = 1.0;
const float BULLET_SCALE = 2.0;
const int num_keys = 7;
const int client_fields = 14 + num_keys;
const int bullet_fields = 6;
const int gun_fields = 4;


HANDLE infoMutex;
bool server;
int msg_num = 0;
int PORT = 8888;
int id = 0;
int object_id;
string server_addr = "127.0.0.1"; //DEFAULT
string default_nick = "Player";

float get_angle(int, int, int, int);
string get_word(string &msg);
float string_to_float(string s);
bool check_collision(int x1left, int x1right, int y1up, int y1down, int x2left, int x2right, int y2up, int y2down);

enum key
{
	UP, DOWN, LEFT, RIGHT, SPACE, LMB, TAB
};
bool keys[num_keys];

class Player;
int string_to_int(string);

class Object
{
public:
	int id;
	float x;
	float y;
	float sx;
	float sy;
	float angle;
	ALLEGRO_BITMAP * texture;
	Object() : id(object_id++), x(0.0), sx(0.0), y(0.0), sy(0.0), angle(0.0) {
		texture = NULL;
	}
	Object(float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP * ab) : id(object_id++), x(_x), sx(_sx), y(_y), sy(_sy), angle(_angle) {
		texture = ab;
	}
	void draw() {
		al_draw_scaled_rotated_bitmap(this->texture, al_get_bitmap_width(texture) / 2.0, al_get_bitmap_height(texture) / 2.0, x, y, sx / al_get_bitmap_width(texture), sy / al_get_bitmap_height(texture), angle, 0);
	}
	float getx1() {
		return x - sx / 2.0;
	}
	float getx2() {
		return x + sx / 2.0;
	}
	float gety1() {
		return y - sy / 2.0;
	}
	float gety2() {
		return y + sy / 2.0;
	}
};

class Bullet : public Object
{
public:
	float speed;
	Bullet * next;
	Bullet * previous;
	Player * owner;
	Bullet(float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP * ab, float _speed, Player * _owner) : Object(_x, _y, _sx, _sy, _angle, ab), speed(_speed), owner(_owner)
	{
		previous = next = NULL;
	}
	Bullet * add(float _x, float _y, float _sx, float _sy, float _angle, float _speed, Player * _owner) {
		Bullet * new_bullet = new Bullet(_x, _y, _sx, _sy, _angle, this->texture, _speed, _owner);
		this->previous->next = new_bullet;
		new_bullet->previous = this->previous;
		new_bullet->next = this;
		this->previous = new_bullet;
		return new_bullet;
	}
	void remove() {
		this->previous->next = this->next;
		this->next->previous = this->previous;
		next = previous= NULL;
		owner = NULL;
		delete this;
	}
	void removeAll() {
		while (this->next != this) {
			this->next->remove();
		}
	}
};

class Gun : public Object
{
public:
	int gid;
	int ammo;
	float cd;
	float ccd;
	Gun() : Object() {}
	Gun(int _gid, float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP * ab, int _ammo, float _cd) : gid (_gid), Object(_x, _y, _sx, _sy, _angle, ab), ammo(_ammo), cd(_cd), ccd(0.0) {}
	virtual void shoot(Bullet * bullets, Player * owner) = 0;
	virtual void reload(float delta) {
		ccd = max(0.0, ccd - delta);
	}
	virtual void draw() {
		al_draw_scaled_rotated_bitmap(texture, al_get_bitmap_width(texture) / 2.0, al_get_bitmap_height(texture) / 2.0, x, y, sx, sy, angle, angle >= 0.5 * PI && angle < 1.5 * PI ? ALLEGRO_FLIP_VERTICAL : 0);
	}
};

class Shotgun : public Gun
{
public:
	Shotgun() : Gun() {}
	Shotgun(float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP * shotgun_texture) : Gun(0, _x, _y, _sx, _sy, _angle, shotgun_texture, 12, 0.8) {}
	void shoot(Bullet * bullets, Player * owner) {
		if (ammo > 0 && ccd <= 0.0) {
			bullets->add(x, y, 1.0, 1.0, angle, 400.0, owner);
			bullets->add(x, y, 1.0, 1.0, angle - PI / 32.0, 400.0, owner);
			bullets->add(x, y, 1.0, 1.0, angle - PI / 16.0, 400.0, owner);
			bullets->add(x, y, 1.0, 1.0, angle + PI / 32.0, 400.0, owner);
			bullets->add(x, y, 1.0, 1.0, angle + PI / 16.0, 400.0, owner);
			--ammo;
			ccd += cd;
		}
	}
};
class AR : public Gun
{
public:
	AR() : Gun() {}
	AR(float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP * ar_texture) : Gun(1, _x, _y, _sx, _sy, _angle, ar_texture, 40, 0.1) {}
	void shoot(Bullet * bullets, Player * owner) {
		if (ammo > 0 && ccd <= 0.0) {
			bullets->add(x, y, 1.0, 1.0, angle, 550.0, owner);
			--ammo;
			ccd += cd;
		}
	}
};

class Player : public Object
{
public:
	float speed;
	float v_speed;
	float mouse_x;
	float mouse_y;
	bool in_air;
	ALLEGRO_BITMAP ** texture;
	Gun * gun;
	Player(float _x, float _y, float _sx, float _sy, float _angle, ALLEGRO_BITMAP ** ab, float _speed, float _v_speed, float _mouse_x, float _mouse_y) : Object(_x, _y, _sx, _sy, _angle, NULL), speed(_speed), v_speed(_v_speed), in_air(false), texture(ab), mouse_x(_mouse_x), mouse_y(_mouse_y), gun(NULL)
	{
	}
	void draw() {
		//TODO
		if(angle < 0.5 * PI) al_draw_scaled_rotated_bitmap(texture[0], al_get_bitmap_width(texture[0]) / 2.0, al_get_bitmap_height(texture[0]) / 2.0, x, y, sx, sy, 0.0,  0);
		else if (angle < PI) al_draw_scaled_rotated_bitmap(texture[0], al_get_bitmap_width(texture[0]) / 2.0, al_get_bitmap_height(texture[0]) / 2.0, x, y, sx, sy, 0.0, ALLEGRO_FLIP_HORIZONTAL);
		else if (angle < 1.5 * PI) al_draw_scaled_rotated_bitmap(texture[0], al_get_bitmap_width(texture[0]) / 2.0, al_get_bitmap_height(texture[0]) / 2.0, x, y, sx, sy, 0.0, ALLEGRO_FLIP_HORIZONTAL);
		else if (angle < 2 * PI) al_draw_scaled_rotated_bitmap(texture[0], al_get_bitmap_width(texture[0]) / 2.0, al_get_bitmap_height(texture[0]) / 2.0, x, y, sx, sy, 0.0, 0);

		if(gun) gun->draw();
	}
	void set_angle() {
		angle = get_angle(x, y, mouse_x, mouse_y);
	}
	void shoot(Bullet * bullets) {
		if (gun) gun->shoot(bullets, this);
	}
	void reload(float delta) {
		if (gun) gun->reload(delta);
	}
};

class Client
{
public:
	int cid;
	string ip;
	int port;
	string nick;
	vector < pair <int, string> > msg;
	Player * player;
	volatile bool ready;
	bool keys[num_keys];
	sockaddr_in sai;
	SOCKET sock;
	int kills;
	int deaths;
	float timeout;
	Client(string _ip, int _port, string _nick, Player * _player, sockaddr_in _sai, SOCKET _sock) : cid(id++), ip(_ip), nick(_nick), port(_port), player(_player), ready(false), sai(_sai), sock(_sock), kills(0), deaths(0), timeout(default_timeout)
	{
		msg.clear();
		for (int i = 0; i < num_keys; ++i) {
			keys[i] = false;
		}
	}
	bool read_msg(Object ** platforms, ALLEGRO_BITMAP * platform_texture, Client ** clients, int &num_clients, ALLEGRO_BITMAP ** player_textures, ALLEGRO_BITMAP * shotgun_texture, ALLEGRO_BITMAP * ar_texture,  Bullet * bullets, Gun ** guns, int &gun_num) {
		WaitForSingleObject(
			infoMutex,    // handle to mutex
			INFINITE);
		sort(msg.begin(), msg.end());
		for (int i = 0; i < msg.size(); ++i) {
			if (i != msg[i].first) {
				ReleaseMutex(infoMutex);
				return false;
			}
			if (i == msg.size() - 1) {
				bool ok = false;
				for (int j = 0; j < BUFLEN; ++j) {
					if (msg[i].second[j] == '\0') {
						ok = true;
						break;
					}
				}
				if (!ok) {
					ReleaseMutex(infoMutex);
					return false;
				}
			}
		}
		string message = "";
		for (int i = 0; i < msg.size(); ++i) {
			message += msg[i].second;
		}
		printf("Recived frame [%s]\n", message.c_str());
		if (!this->ready) {
			string plat[5];
			string ss;
			int pnum = 0;
			while ((ss = get_word(message)).length() > 0) {
				if (!ss.compare(string("CID"))) {
					this->cid = string_to_int(get_word(message));
				}
				if (ss[0] == 'p' && ss.length() == 1) {
					plat[0] = ss;
					plat[1] = get_word(message);
					plat[2] = get_word(message);
					plat[3] = get_word(message);
					plat[4] = get_word(message);
					platforms[pnum] = new Object(string_to_float(plat[1]), string_to_float(plat[2]), string_to_float(plat[3]), string_to_float(plat[4]), 0.0, platform_texture);
					++pnum;
				}
			}
			this->ready = true;
			msg.clear();
			ReleaseMutex(infoMutex);
			return true;
		}
		else {
			string tag = get_word(message);
			if (!tag.compare(string("INFO"))) {
				bullets->removeAll();
				string client[client_fields];
				string bullet[bullet_fields];
				string gun[gun_fields];
				string ss;
				num_clients = 0;
				gun_num = 0;
				int index = 2;
				Client * this_client = NULL;
				while ((ss = get_word(message)).length() > 0) {
					if (!ss.compare(string("c"))) {
						for (int i = 0; i < client_fields; ++i) {
							client[i] = get_word(message);
						}
						//client
						int _cid = string_to_int(client[0]);
						this_client = NULL;
						if (_cid == clients[0]->cid) this_client = clients[0];   
						else if (_cid == clients[1]->cid) this_client = clients[1];
						else if (clients[index] && _cid == clients[index]->cid) this_client = clients[index++];
						else ++index;

						if (!this_client) {
							this_client = clients[index - 1] = new Client("", -1, client[1], new Player(float(0.0), float(0.0), SCALE, SCALE, float(0.0), player_textures, 300.0, 0.0, SCREEN_W, SCREEN_H / 2.0), *(new sockaddr_in), NULL);
							this_client->cid = _cid;
						}
						++num_clients;
						this_client->nick = client[1];
						int i = 2;
						for (; i < 2 + num_keys; ++i) {
							if (this_client != this) this_client->keys[i - 2] = string_to_int(client[i]);
						}
						this_client->kills = string_to_float(client[i++]);
						this_client->deaths = string_to_float(client[i++]);
						//player
						this_client->player->id = string_to_float(client[i++]);
						this_client->player->x = string_to_float(client[i++]);
						this_client->player->y = string_to_float(client[i++]);
						this_client->player->v_speed = string_to_float(client[i++]);
						this_client->player->in_air = string_to_int(client[i++]);
						if (this_client != this) {
							this_client->player->mouse_x = string_to_int(client[i++]);
							this_client->player->mouse_y = string_to_int(client[i++]);
						}
						else i += 2;
						//gun
						if (string_to_float(client[i]) != -1) {
							if (!this_client->player->gun) {
								if (string_to_float(client[i]) == 0) {
									this_client->player->gun = new Shotgun(0.0, 0.0, SCALE, SCALE, 0.0, shotgun_texture);
								}
								else if (string_to_float(client[i]) == 1) {
									this_client->player->gun = new AR(0.0, 0.0, SCALE, SCALE, 0.0, ar_texture);
								}
							}
							++i;
							this_client->player->gun->angle = string_to_float(client[i++]);
							this_client->player->gun->ccd = string_to_float(client[i++]);
						}
					}
					if (!ss.compare(string("b"))) {
						for (int i = 0; i < bullet_fields; ++i) {
							bullet[i] = get_word(message);
						}
						Player * owner = NULL;
						for (int i = 0; i < num_clients; ++i) {
							if (string_to_int(bullet[5]) == clients[i]->player->id) {
								owner = clients[i]->player;
								break;
							}
						}
						Bullet * this_bullet = bullets->add(string_to_float(bullet[1]), string_to_float(bullet[2]), SCALE, SCALE, string_to_float(bullet[4]), string_to_float(bullet[3]), owner);
						this_bullet->id = string_to_int(bullet[0]);

					}
					if (!ss.compare(string("g"))) {
						for (int i = 0; i < gun_fields; ++i) {
							gun[i] = get_word(message);
						}
						Gun * this_gun = guns[gun_num];
						if (!this_gun || this_gun->id != string_to_int(gun[0])) {
							if (gun_num == max_guns) {
								printf("[ERROR]New gun at %d\n", gun_num);
								for (int i = 0; i < gun_num; ++i) {
									printf("ID[%d] = %d\n", i, guns[i]->id);
								}
								for (int i = 0; i < msg.size(); ++i) {
									printf("Dump: [%s]\n", msg[i].second);
								}
							}
							if (string_to_int(gun[1]) == 0) {
								this_gun = guns[gun_num] = new Shotgun(0.0, 0.0, SCALE, SCALE, 0.0, shotgun_texture);
								this_gun->id = string_to_int(gun[0]);
							}
							else if (string_to_int(gun[1]) == 1) {
								this_gun = guns[gun_num] = new AR(0.0, 0.0, SCALE, SCALE, 0.0, ar_texture);
								this_gun->id = string_to_int(gun[0]);
							}
						}
						++gun_num;
						this_gun->x = string_to_float(gun[2]);
						this_gun->y = string_to_float(gun[3]);
					}
				}
				msg.clear();
				ReleaseMutex(infoMutex);
				return true;
			}
		}
	}
	void draw(ALLEGRO_FONT * font) {
		player->draw();
		al_draw_text(font, al_map_rgb(0, 0, 0), player->x, player->y - 60, ALLEGRO_ALIGN_CENTER, nick.c_str());
	}
	void shoot(Bullet * bullets) {
		if (keys[LMB]) player->shoot(bullets);
	}
};

void spawn_guns(Gun ** guns, ALLEGRO_BITMAP * shotgun_texture, ALLEGRO_BITMAP * ar_texture, Object ** platforms, int num_platforms, int & num_guns, int max_guns, float & cooldown, float delta) {
	cooldown = max(cooldown - delta, 0.0);
	if (cooldown == 0.0 && num_guns < max_guns) {
		int r_x, r_y;
		while (true) {
			r_x = rand() % (SCREEN_W - 100) + 50;
			r_y = rand() % (SCREEN_H - 150) + 50;
			bool ok_to_spawn = true;
			for (int i = 0; i < num_platforms; ++i) {
				if (check_collision(r_x, r_x, r_y, r_y, platforms[i]->getx1(), platforms[i]->getx2(), platforms[i]->gety1(), platforms[i]->gety2())) {
					ok_to_spawn = false;
					break;
				}
			}
			if (ok_to_spawn) break;
		}
		int r_type = rand() % 2;
		if (r_type == 0) {
			guns[num_guns++] = new Shotgun(r_x, r_y, SCALE, SCALE, 0.0, shotgun_texture);
		}
		else if (r_type == 1) {
			guns[num_guns++] = new AR(r_x, r_y, SCALE, SCALE, 0.0, ar_texture);
		}
		cooldown = 5.0;
	}
}

void remove_client(Client ** clients, int ind, int &num_clients) {
	for (int i = ind; i < num_clients - 1; ++i) {
		clients[i] = clients[i + 1];
	}
	--num_clients;
}

//NOT USED
ALLEGRO_BITMAP * color_bitmap(ALLEGRO_BITMAP * orgin) {
	ALLEGRO_BITMAP * copy = NULL;
	copy = al_clone_bitmap(orgin);
	al_lock_bitmap(orgin, al_get_bitmap_format(copy), ALLEGRO_LOCK_READWRITE);
	unsigned char r = 0, g = 0, b = 0;
	al_set_target_bitmap(copy);
	for (int i = 0; i < al_get_bitmap_width(copy); ++i) {
		for (int j = 0; j < al_get_bitmap_height(copy); ++j) {
			al_unmap_rgb(al_get_pixel(copy, i, j), (unsigned char *) &r, (unsigned char *) &g, (unsigned char *) &b);
			if (r == 0 && g == 0 && b == 0) { printf("%d, %d, %d", r, g, b); }//al_put_pixel(i, j, al_map_rgb(100, 100, 0));
		}
	}
	al_unlock_bitmap(copy);
	return copy;
}

string get_word(string &msg) {
	string res = "";
	int i;
	for (i = 0; i < msg.length(); ++i) {
		if (msg[i] == ':') break;
		res += msg[i];
	}
	//printf("%d >= %d = %d", i, msg.length() - 1, i >= (int(msg.length()) - 1));
	if (i >= int(msg.length()) - 1) {
		msg = "";
	}
	else {
		msg = msg.substr(i + 1, msg.length());
	}
	return res;
}

string float_to_string(float x) {
	bool minus = false;
	if (x < 0.0) {
		minus = true;
		x = -x;
	}
	int a, b;
	a = floor(x);
	float man = x - float(a);
	man *= 10000.0;
	b = round(man);
	string result = "";
	string pom = "";
	while (a > 0) {
		result = char((a % 10) + 48) + result;
		a /= 10;
	}
	result += ".";
	int dig = 4;
	while (dig > 0) {
		pom= char((b % 10) + 48) + pom;
		b /= 10;
		--dig;
	}
	return (minus ? string("-") : string("")) + result + pom;
}

float string_to_float(string s) {
	float sign = 1.0;
	bool man = false;
	float div = 10.0;
	float result = 0.0;
	int i = 0;
	if (s[0] == '-') {
		++i;
		sign = -1.0;
	}
	for (; i < s.length(); ++i) {
		if (s[i] != '.') {
			if (!man) {
				result *= 10.0;
				result += float(int(s[i]) - 48);
			}
			else {
				result += float(int(s[i]) - 48) / div;
				div *= 10.0;
			}
		}
		else {
			man = true;
		}
	}
	return sign * result;
}

string int_to_string(int a) {
	bool minus = false;
	if (a < 0) {
		minus = true;
		a = -a;
	}
	string result = "";
	while (a > 0) {
		result = char((a % 10) + 48) + result;
		a /= 10;
	}
	if (result.length() == 0) {
		result = "0";
	}
	return (minus ? string("-") : string("")) + result;
}

int string_to_int(string s) {
	int sign = 1;
	int i = 0;
	if (s[0] == '-') {
		sign = -1;
		++i;
	}
	int result = 0;
	for (; i < s.length(); ++i) {
		result *= 10;
		result += int(s[i]) - 48;
	}
	return result;
}

bool check_header(char * buf, char * text, int len) {
	return !string(buf).substr(0, len).compare(string(text).substr(0, len));
}

bool send_datagrams(SOCKET s, string str, sockaddr_in sin, int slen) {
	int my_num = 0;
	int pnum = 0;
	char * buf = new char[512];
	my_num = msg_num++;
	str += '\0';
	while (str.length() > 0) {
		string msg = string("MSG:") + int_to_string(my_num) + string(":") + int_to_string(pnum) + string(":");
		int header_size = msg.size();
		++pnum;
		msg += str.substr(0, min(512 - header_size, str.length()));
		str = str.substr(min(512 - header_size, str.length()), str.length());
		memset(buf, '\0', BUFLEN);
		for (int i = 0; i < msg.length(); ++i) {
			buf[i] = msg[i];
		}
		if(DEBUG) printf("[Debug] Sending datagram: %s\n", buf);
		if (sendto(s, buf, BUFLEN, 0, (struct sockaddr*) &sin, slen) == SOCKET_ERROR)
		{
			printf("sendto() failed with error code : %d", WSAGetLastError());
			exit(EXIT_FAILURE);
		}
	}
}

struct thrd_args 
{
	Client ** clients;
	int * num_clients;
	ALLEGRO_BITMAP ** textures;
	int  * num_textures;
	Object ** platforms;
	int  * num_platforms;
	ALLEGRO_BITMAP * platform_texture;
	ALLEGRO_BITMAP * shotgun_texture;
	ALLEGRO_BITMAP * ar_texture;
	Bullet * bullet;
	Gun ** guns;
	int * num_guns;
};

DWORD WINAPI run_server(LPVOID lpParameter)
{
	thrd_args * args = (thrd_args*)lpParameter;
	Client ** clients = args->clients;
	int * num_clients = args->num_clients;
	ALLEGRO_BITMAP ** textures = args->textures;
	int * num_textures = args->num_textures;
	Object ** platforms = args->platforms;
	int * num_platforms = args->num_platforms;

	SOCKET s;
	struct sockaddr_in server, si_other;
	int slen, recv_len;
	char buf[BUFLEN];
	WSADATA wsa;

	slen = sizeof(si_other);

	//Initialise winsock
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");

	//Create a socket
	if ((clients[0]->sock = s = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}
	printf("Socket created.\n");

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);

	//Bind
	if (bind(s, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	puts("Bind done");

	clients[0]->sai = server;

	//keep listening for data
	while (1)
	{
		printf("Waiting for data...");
		fflush(stdout);

		//clear the buffer by filling null, it might have previously received data
		memset(buf, '\0', BUFLEN);

		//try to receive some data, this is a blocking call
		if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == SOCKET_ERROR)
		{
			printf("recvfrom() failed with error code : %d\n", WSAGetLastError());
			continue;
		}

		if (recv_len < BUFLEN) {
			printf("datagram is too short!\n");
			continue;
		}

		//print details of the client/peer and the data received
		printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
		if(!DEBUG) printf("Data: %s\n", buf);

		int ind = -1;

		Client * this_client = NULL;
		for (int i = 1; i < *num_clients; ++i) {
			if (!string(clients[i]->ip.c_str()).compare(string(inet_ntoa(si_other.sin_addr))) && clients[i]->port == ntohs(si_other.sin_port)) {
				this_client = clients[i];
				ind = i;
				break;
			}
		}
		if (this_client == NULL) {
			if (*num_clients == max_clients) {
				continue;
			}
			if (!check_header(buf, "JOIN", 4))
			{
				continue;
			}
			string join = buf;
			get_word(join);
			string nick = get_word(join);
			clients[*num_clients] = new Client(inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), nick, new Player(rand() % (SCREEN_W - 100) + 50, 0, SCALE, SCALE, 0.0, textures/*TODO*/, 300.0, 0.0, SCREEN_W / 2.0, SCREEN_H / 2.0), si_other, NULL);
			++(*num_clients);
			/*temp*/
			
			string str = "CID:";
			str += int_to_string(clients[*num_clients - 1]->cid) + ":";
			str += "MAP:";
			for (int i = 0; i < *num_platforms; ++i) {
				str += "p:";
				str += float_to_string(platforms[i]->x) + ":";
				str += float_to_string(platforms[i]->y) + ":";
				str += float_to_string(platforms[i]->sx) + ":";
				str += float_to_string(platforms[i]->sy) + ":";

			}
			if (!send_datagrams(s, str, si_other, slen)) {
				--*num_clients;
				delete clients[*num_clients];
				clients[*num_clients] = NULL;
				continue;
			}
			clients[*num_clients - 1]->ready = true;
		}
		else {
			//NotGood
			this_client->timeout = default_timeout;
			int i = 0;
			int c = 0;
			for (; i < BUFLEN; ++i) {
				if (buf[i] == ':') ++c;
				if (c == 3) break;
			}
			char *buf2 = buf + min(i + 1, BUFLEN - 1);
			if(DEBUG) printf("Message = %s\n", buf2);
			if (!this_client->ready) {
				if (check_header(buf2, "ACK", 3)) {
					this_client->ready = true;
				}
				continue;
			}
			if (check_header(buf2, "MOUSE", 3)) {
				string str = buf2;
				get_word(str);
				int mx = string_to_int(get_word(str));
				int my = string_to_int(get_word(str));
				printf("%d, %d\n", mx, my);
				WaitForSingleObject(infoMutex, INFINITE);
				this_client->player->mouse_x = mx;
				this_client->player->mouse_y = my;
				ReleaseMutex(infoMutex);
			}
			if (check_header(buf2, "BTNUP", 5)) {
				int button = int(buf2[6]) - 48;
				this_client->keys[button] = false;
			}
			if (check_header(buf2, "BTNDOWN", 7)) {
				int button = int(buf2[8]) - 48;
				this_client->keys[button] = true;
			}
			if (check_header(buf2, "EXIT", 4)) {
				remove_client(clients, ind, *num_clients);
			}
		}
	}

	closesocket(s);
	WSACleanup();
	return 0;
}

DWORD WINAPI run_client(LPVOID lpParameter) {

	thrd_args * args = (thrd_args*)lpParameter;
	Client ** clients = args->clients;
	int * num_clients = args->num_clients;
	ALLEGRO_BITMAP ** textures = args->textures;
	int * num_textures = args->num_textures;
	Object ** platforms = args->platforms;
	int * num_platforms = args->num_platforms;
	ALLEGRO_BITMAP * platform_texture = args->platform_texture;
	ALLEGRO_BITMAP * shotgun_texture = args->shotgun_texture;
	ALLEGRO_BITMAP * ar_texture = args->ar_texture;
	Bullet * bullet = args->bullet;
	Gun ** guns = args->guns;
	int * num_guns = args->num_guns;

	struct sockaddr_in si_other;
	SOCKET s;
	int slen = sizeof(si_other);
	char buf[BUFLEN];
	//char message[BUFLEN];
	WSADATA wsa;
	int recv_len = 0;

	//Initialise winsock
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");

	//create socket
	if ((clients[0]->sock = s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		printf("socket() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	//setup address structure
	memset((char *)&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	si_other.sin_addr.S_un.S_addr = inet_addr(server_addr.c_str());

	//JOIN the server
	string join = "JOIN:";
	join += clients[0]->nick + ":";
	memset(buf, '\0', BUFLEN);
	for (int i = 0; i < join.size(); ++i) buf[i] = join[i];
	if (DEBUG) printf("sending [%s]\n", buf);
	if (sendto(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, slen) == SOCKET_ERROR)
	{
		printf("sendto() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	clients[0]->msg.clear();


	clients[1] = new Client(server_addr, PORT, "", new Player(0.0, 0.0, SCALE, SCALE, 0.0, textures, 300.0, 0.0, SCREEN_W, SCREEN_H / 2.0), si_other, NULL);
	clients[1]->cid = 0;
	++*num_clients;

	//start communication
	while (1)
	{
		//receive a reply and print it
		//clear the buffer by filling null, it might have previously received data
		memset(buf, '\0', BUFLEN);
		//try to receive some data, this is a blocking call
		if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == SOCKET_ERROR)
		{
			printf("recvfrom() failed with error code : %d", WSAGetLastError());
			continue;
		}
		clients[1]->timeout = default_timeout;
		if(DEBUG) printf("received [%s]\n", buf);
		if (recv_len < BUFLEN) {
			printf("datagram is too short");
			continue;
		}
		if (!clients[0]->ready) {
			if (check_header(buf, "MSG", 3)) {
				int i = 0;
				int c = 0;
				string nr = "";
				for (; i < BUFLEN; ++i) {
					if (c == 2 && buf[i] != ':') {
						nr += buf[i];
					}
					if (c == 3) {
						break;
					}
					if (buf[i] == ':') {
						++c;
					}
				}
				clients[0]->msg.push_back(make_pair(string_to_int(nr), string(buf).substr(i, BUFLEN)));
				if (clients[0]->read_msg(platforms, platform_texture, clients, *num_clients, textures, shotgun_texture, ar_texture, bullet, guns, *num_guns)) {


				}
			}
		}
		else {
			if (check_header(buf, "MSG", 3)) {
				int i = 0;
				int c = 0;
				string nr = "";
				for (; i < BUFLEN; ++i) {
					if (c == 2 && buf[i] != ':') {
						nr += buf[i];
					}
					if (c == 3) {
						break;
					}
					if (buf[i] == ':') {
						++c;
					}
				}
				clients[0]->msg.push_back(make_pair(string_to_int(nr), string(buf).substr(i, BUFLEN)));
				if (clients[0]->read_msg(platforms, platform_texture, clients, *num_clients, textures, shotgun_texture, ar_texture, bullet, guns, *num_guns)) {
				}
				else {

				}
			}
		}

	}

	closesocket(s);
	WSACleanup();

	return 0;
}

float get_angle(int sx, int sy, int x, int y)
{
	if (y <= sy && x < sx)
	{
		return asin(abs(sy - y) / sqrt(pow(sy - y, 2) + pow(sx - x, 2))) + 1.0 * PI;
	}
	else if (y < sy && x >= sx)
	{
		return asin(abs(sx - x) / sqrt(pow(sy - y, 2) + pow(sx - x, 2))) + 1.5 * PI;
	}
	else if (y >= sy && x > sx)
	{
		return asin(abs(sy - y) / sqrt(pow(sy - y, 2) + pow(sx - x, 2)));
	}
	else if (y > sy && x <= sx)
	{
		return asin(abs(sx - x) / sqrt(pow(sy - y, 2) + pow(sx - x, 2))) + 0.5 * PI;
	}
	else {
		return 0.0;
	}
}

bool check_collision(int x1left, int x1right, int y1up, int y1down, int x2left, int x2right, int y2up, int y2down) {
	if (x1left <= x2right &&
		x1right >= x2left &&
		y1up <= y2down &&
		y1down >= y2up) {
		return true;
	}
	return false;
}

void kill_player(Client * killer, Client * victim) {
	++killer->kills;
	++victim->deaths;
	victim->player->x = rand() % (SCREEN_W - 100) + 50;
	victim->player->y = 0.0;
	if(victim->player->gun) victim->player->gun->ccd = 1.5;
}

void move_bullets(Bullet * bullets, double delta, Client ** clients, int num_clients, Object ** platforms, int num_platforms) {
	for (Bullet * b = bullets->next; b != bullets; b = b->next) {
		b->x += cos(b->angle) * b->speed * delta;
		b->y += sin(b->angle) * b->speed * delta;
		if (b->x < 0 || b->x >= SCREEN_W || b->y < 0 || b->y >= SCREEN_H) {
			Bullet * pom = b->previous;
			b->remove();
			b = pom;
			continue;
		}
		for (int i = 0; i < num_clients; ++i) {
			if (b->owner != clients[i]->player) {
				if (check_collision(b->x, b->x, b->y, b->y,
					clients[i]->player->x - al_get_bitmap_width(clients[i]->player->texture[0]) / 2.0, clients[i]->player->x + al_get_bitmap_width(clients[i]->player->texture[0]) / 2.0, clients[i]->player->y - al_get_bitmap_height(clients[i]->player->texture[0]) / 2.0, clients[i]->player->y + al_get_bitmap_height(clients[i]->player->texture[0]) / 2.0)) {
					if (server) {
						Client * killer = NULL;
						for (int j = 0; j < num_clients; ++j) {
							if (clients[j]->player == b->owner) {
								killer = clients[j];
								break;
							}
						}
						if (killer) {
							if (DEBUG) printf("[Debug] [%s] shot [%s]\n", killer->nick, clients[i]->nick);
						}
						kill_player(killer, clients[i]);
					}
					Bullet * pom = b->previous;
					b->remove();
					b = pom;
					continue;
				}
			}
			}
		for (int i = 0; i < num_platforms; ++i) {
			if (check_collision(b->x, b->x, b->y, b->y,
				platforms[i]->x - platforms[i]->sx / 2.0, platforms[i]->x + platforms[i]->sx / 2.0, platforms[i]->y - platforms[i]->sy / 2.0, platforms[i]->y + platforms[i]->sy / 2.0)) {
				Bullet * pom = b->previous;
				b->remove();
				b = pom;
				continue;
			}
		}
	}
}

void draw_bullets(Bullet * bullets) {
	for (Bullet * b = bullets->next; b != bullets; b = b->next) {
		al_draw_scaled_rotated_bitmap(b->texture, al_get_bitmap_width(b->texture) / 2.0, al_get_bitmap_height(b->texture) / 2.0, b->x, b->y, b->sx * BULLET_SCALE, b->sy * BULLET_SCALE, b->angle, 0);
	}
}

void move(Client * client, double delta, Object * list[], int num, Gun ** guns, int &num_guns) {
	if (delta < 0.0) return;
	if (client->keys[LEFT]) {
		client->player->x -= client->player->speed * delta;
	}
	if (client->keys[RIGHT]) {
		client->player->x += client->player->speed * delta;
	}
	client->player->x = min(int(client->player->x), SCREEN_W - 1 - 8);
	client->player->x = max(int(client->player->x), 0 + 8);
	if (client->keys[SPACE] && !client->player->in_air) {
		client->player->v_speed = -800.0;
		client->player->in_air = true;
	}
	if (client->player->in_air) {
		float gravity = 2000.0;
		float old_y = client->player->y;
		client->player->y += client->player->v_speed * delta + gravity * pow(delta, 2);
		client->player->v_speed = client->player->v_speed + gravity * delta;
		if (client->player->y >= SCREEN_H - 1 - 24) {
			client->player->y = SCREEN_H - 1 - 24;
			client->player->in_air = false;
			client->player->v_speed = 0.0;
		}
		for (int i = 0; i < num; ++i) {
			if (list[i]->getx1() <= client->player->x + 8.0 && list[i]->gety1() <= client->player->y + 24.0 && list[i]->getx2() >= client->player->x - 8.0 && list[i]->gety1() >= client->player->y - 24.0 && old_y < client->player->y && old_y <= list[i]->gety1() - 24.0) {
				client->player->y = list[i]->gety1() - 24.0;
				client->player->in_air = false;
				client->player->v_speed = 0.0;
				break;
			}
		}
	}
	else {
		bool ok = false;
		for (int i = 0; i < 5; ++i) {
			if (list[i]->getx1() <= client->player->x + 8.0 && list[i]->gety1() <= client->player->y + 1 + 24.0 && list[i]->getx2() >= client->player->x - 8.0 && list[i]->gety2() >= client->player->y - 1.0 - 24.0) {
				ok = true;
				break;
			}
		}
		if (!ok) {
			client->player->v_speed = 0.0;
			client->player->in_air = true;
		}
	}
	for (int i = 0; i < num_guns; ++i) {
		if (guns[i]) {
			if (check_collision(client->player->x - SCALE * al_get_bitmap_width(client->player->texture[0]) / 2.0, client->player->x + SCALE * al_get_bitmap_width(client->player->texture[0]) / 2.0,
				client->player->y - SCALE * al_get_bitmap_height(client->player->texture[0]) / 2.0, client->player->y + SCALE * al_get_bitmap_height(client->player->texture[0]) / 2.0,
				guns[i]->x - SCALE * al_get_bitmap_width(guns[i]->texture) / 2.0, guns[i]->x + SCALE * al_get_bitmap_width(guns[i]->texture) / 2.0,
				guns[i]->y - SCALE * al_get_bitmap_height(guns[i]->texture) / 2.0, guns[i]->y + SCALE * al_get_bitmap_height(guns[i]->texture) / 2.0)) {
				client->player->gun = guns[i];
				for (int j = i; j < num_guns - 1; ++j) guns[j] = guns[j + 1];
				--num_guns;
			}
		}
	}
}

void update_gun(Client * client) {
	if (client->player->gun) {
		client->player->gun->x = client->player->x;
		client->player->gun->y = client->player->y;
		client->player->gun->angle = client->player->angle;
	}
}

string create_info(Client ** clients, int num_clients, Bullet * bullet, Gun ** guns, int gun_num) {
	string result = "";
	result += string("INFO:") + string("CNUM:") + int_to_string(num_clients) + string(":");
	//client
	for (int i = 0; i < num_clients; ++i) {
		result += string("c:");
		result += int_to_string(clients[i]->cid) + ":";
		result += clients[i]->nick + ":";
		for (int j = 0; j < num_keys; ++j) {
			result += int_to_string(int(clients[i]->keys[j])) + ":";
		}
		result += int_to_string(clients[i]->kills) + ":";
		result += int_to_string(clients[i]->deaths) + ":";
		//player
		result += int_to_string(clients[i]->player->id) + ":";
		result += float_to_string(clients[i]->player->x) + ":";
		result += float_to_string(clients[i]->player->y) + ":";
		result += float_to_string(clients[i]->player->v_speed) + ":";
		result += int_to_string(int(clients[i]->player->in_air)) + ":";
		result += int_to_string(clients[i]->player->mouse_x) + ":";
		result += int_to_string(clients[i]->player->mouse_y) + ":";

		//guns
		if(clients[i]->player->gun) result += int_to_string(clients[i]->player->gun->gid) + ":";
		else result += int_to_string(-1) + ":";
		if (clients[i]->player->gun) result += float_to_string(clients[i]->player->gun->angle) + ":";
		else result += float_to_string(0.0) + ":";
		if (clients[i]->player->gun) result += float_to_string(clients[i]->player->gun->ccd) + ":";
		else result += float_to_string(0.0) + ":";
	}
	for (Bullet * b = bullet->next; b != bullet; b = b->next) {
		result += "b:";
		result += int_to_string(b->id) + ":";
		result += float_to_string(b->x) + ":";
		result += float_to_string(b->y) + ":";
		result += float_to_string(b->speed) + ":";
		result += float_to_string(b->angle) + ":";
		result += int_to_string(b->owner->id) + ":";

	}
	for (int i = 0; i < gun_num; ++i) {
		if (guns[i]) {
			result += "g:";
			result += int_to_string(guns[i]->id) + ":";
			result += int_to_string(guns[i]->gid) + ":";
			result += float_to_string(guns[i]->x) + ":";
			result += float_to_string(guns[i]->y) + ":";
		}
	}
	return result;
}

void write_scores(Client ** clients, int num_clients, ALLEGRO_FONT * font, int x1, int x2, int y) {
	al_draw_filled_rectangle(x1 - 10, y - 10, x2 + 10, y + SCALE * 300, al_map_rgb(150, 150, 150));
	int lh = al_get_font_line_height(font);
	al_draw_text(font, al_map_rgb(200, 0, 0), x2 - 10, y, ALLEGRO_ALIGN_CENTER, "D");
	al_draw_text(font, al_map_rgb(0, 200, 0), x2 - 10 - al_get_text_width(font, "   K"), y, ALLEGRO_ALIGN_CENTER, "K");
	for (int i = 0; i < num_clients; ++i) {
		al_draw_text(font, al_map_rgb(0, 0, 0), x1, y + (i + 1) * lh, ALLEGRO_ALIGN_LEFT, clients[i]->nick.c_str());
		al_draw_text(font, al_map_rgb(200, 0, 0), x2 - 10, y + (i + 1) * lh, ALLEGRO_ALIGN_CENTER, int_to_string(clients[i]->deaths).c_str());
		al_draw_text(font, al_map_rgb(0, 200, 0), x2 - 10 - al_get_text_width(font, "   K"), y + (i + 1) * lh, ALLEGRO_ALIGN_CENTER, int_to_string(clients[i]->kills).c_str());
	}
}

int main()
{
	srand(time(NULL));

	ALLEGRO_DISPLAY* display = NULL;
	ALLEGRO_FONT* font = NULL;
	ALLEGRO_FONT* font_nick = NULL;
	ALLEGRO_EVENT_QUEUE *queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_TIMER *data_timer = NULL;
	ALLEGRO_BITMAP *shotgun = NULL;
	ALLEGRO_BITMAP *ar_texture = NULL;
	ALLEGRO_BITMAP *player_bitmap = NULL;
	ALLEGRO_BITMAP *platform = NULL;
	ALLEGRO_BITMAP *bullet = NULL;
	ALLEGRO_BITMAP * player_textures[1];
	SOCKET * main_socket;
	bool redraw = true;
	double time_old = 0.0;
	double time = 0.0;
	double data_time;
	double data_time_old;
	Client * clients[max_clients];
	int num_clients = 1;
	int num_textures = 1;
	int num_platforms = 5;
	int num_guns = 0;
	float current_fps;
	float gun_spawn_cooldown = 0.0;

	infoMutex = CreateMutex(
		NULL,              // default security attributes
		FALSE,             // initially not owned
		NULL);


	char decision;
	while (1) {
		cout << "(s/c): ";
		cin >> decision;
		if (decision == 's') {
			server = true;
			break;
		}
		if (decision == 'c') {
			server = false;
			break;
		}
	}

	cout << "Podaj adres servera: ";
	cin >> server_addr;
	cout << "Podaj port: ";
	cin >> PORT;
	cout << "Podaj nick: ";
	cin >> default_nick;

	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}
	if (!al_init_primitives_addon()) {
		fprintf(stderr, "failed to primitives addon!\n");
		return -1;
	}
	if (!al_install_mouse()) {
		fprintf(stderr, "failed to initialize the mouse!\n");
		return -1;
	}
	if (!al_install_keyboard()) {
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return -1;
	}
	if (!al_init_image_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_image_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	if (!al_init_font_addon()) {
		fprintf(stderr, "failed to initialize font addon!\n");
		return -1;
	}
	if (!al_init_ttf_addon()) {
		fprintf(stderr, "failed to initialize ttf addon!\n");
		return -1;
	}

	font = al_load_ttf_font("Roboto-Bold.ttf", 16, 0);
	font_nick = al_load_ttf_font("Roboto-Bold.ttf", 24, 0);
	if (!font || !font_nick) {
		fprintf(stderr, "Could not load font.\n");
		return -1;
	}

	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		fprintf(stderr, "failed to create timer!\n");
		return -1;
	}
	data_timer = al_create_timer(data_rate);
	if (!data_timer) {
		fprintf(stderr, "failed to create timer!\n");
		return -1;
	}
	display = al_create_display(SCREEN_W, SCREEN_H);
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}
	queue = al_create_event_queue();
	if (!queue) {
		fprintf(stderr, "failed to create event_queue!\n");
		al_destroy_display(display);
		return -1;
	}
	shotgun = al_load_bitmap("shotgun.png");
	ar_texture = al_load_bitmap("ar.png");
	player_bitmap = al_load_bitmap("Player.png");
	platform = al_load_bitmap("platforma.png");
	bullet = al_load_bitmap("naboj.png");
	if (!shotgun || !player_bitmap || !platform || !bullet || !ar_texture) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load image!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		al_destroy_display(display);
		return 0;
	}

	for (int i = 0; i < max_clients; ++i) {
		clients[i] = NULL;
	}

	Object ** platforms = new Object*[num_platforms];
	platforms[0] = new Object(600, 500, 200, 50, 0.0, platform);
	platforms[1] = new Object(200, 450, 200, 50, 0.0, platform);
	platforms[2] = new Object(500, 400, 200, 50, 0.0, platform);
	platforms[3] = new Object(600, 250, 200, 50, 0.0, platform);
	platforms[4] = new Object(200, 250, 200, 50, 0.0, platform);

	Bullet * bullets = new Bullet((float)0, 0, 0, 0, 0, bullet, 0, (Player *)NULL);
	bullets->next = bullets;
	bullets->previous = bullets;

	Gun * guns[5];
	guns[0] = NULL;// new AR(650, 570, SCALE, SCALE, 0.0, ar_texture);
	guns[1] = NULL;
	guns[2] = NULL;
	guns[3] = NULL;
	guns[4] = NULL;

	player_textures[0] = player_bitmap; //TEMP

	//font = al_create_builtin_font();

	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_timer_event_source(timer));
	al_register_event_source(queue, al_get_timer_event_source(data_timer));
	al_register_event_source(queue, al_get_mouse_event_source());
	al_register_event_source(queue, al_get_keyboard_event_source());

	al_clear_to_color(al_map_rgb(255, 255, 255));

	clients[0] = new Client("127.0.0.1", PORT, default_nick, new Player(SCREEN_W / 2.0, SCREEN_H / 2.0, SCALE, SCALE, 0.0, player_textures, 300.0, 0.0, SCREEN_W, SCREEN_H / 2.0), *(new sockaddr_in), SOCKET(NULL));
	//clients[0]->player->gun = new Shotgun(clients[0]->player->x, clients[0]->player->y, clients[0]->player->sx, clients[0]->player->sy, 0.0, shotgun);

	printf("???: %d", clients[0]->player->x);

	thrd_args  * arguments = new thrd_args();
	arguments->clients = clients;
	arguments->num_clients = &num_clients;
	arguments->platforms = platforms;
	arguments->num_platforms = &num_platforms;
	arguments->textures = player_textures;
	arguments->num_textures = &num_textures;
	arguments->platform_texture = platform;
	arguments->shotgun_texture = shotgun;
	arguments->ar_texture = ar_texture;
	arguments->bullet = bullets;
	arguments->guns = guns;
	arguments->num_guns = &num_guns;


	if (server) {
		DWORD serverThreadID;
		HANDLE serverHandle = CreateThread(0, 0, run_server, arguments, 0, &serverThreadID);
	}
	else {
		DWORD serverThreadID;
		HANDLE serverHandle = CreateThread(0, 0, run_client, arguments, 0, &serverThreadID);
		while (!clients[0]->ready) {}
	}

	al_flip_display();

	al_start_timer(timer);

	al_start_timer(data_timer);

	data_time_old = time_old = al_get_time();

	while (1)
	{
		ALLEGRO_EVENT ev;

		al_wait_for_event(queue, &ev);

		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			if (ev.timer.source == timer) {
				if(!server) WaitForSingleObject(infoMutex, INFINITE);
				redraw = true;
				time = al_get_time();
				//printf("FPS: %f / (%f - %f) = %f\n", 1.0, time, time_old, round(1.0 / (time - time_old)));
				current_fps = 1.0 / (time - time_old);
				for (int i = 0; i < num_clients; ++i) {
					clients[i]->player->set_angle();
					move(clients[i], time - time_old, platforms, num_platforms, guns, num_guns);
					clients[i]->player->reload(time - time_old);
					clients[i]->shoot(bullets);
					update_gun(clients[i]);
				}
				move_bullets(bullets, time - time_old, clients, num_clients, platforms, num_platforms);
				if (server) spawn_guns(guns, shotgun, ar_texture, platforms, num_platforms, num_guns, max_guns, gun_spawn_cooldown, time - time_old);
				if(!server) ReleaseMutex(infoMutex);
			}
			else if (ev.timer.source == data_timer) {
				data_time = al_get_time();
				if (server) {
					string msg = create_info(clients, num_clients, bullets, guns, num_guns);
					for (int i = 1; i < num_clients; ++i) send_datagrams(clients[0]->sock, msg, clients[i]->sai, sizeof(clients[i]->sai));
					WaitForSingleObject(infoMutex, INFINITE);
					for (int i = 1; i < num_clients; ++i) {
						clients[i]->timeout = max(clients[i]->timeout - data_time + data_time_old, 0.0);
						if(DEBUG) printf("Timeoutp[%f]\n", clients[i]->timeout);
						if (clients[i]->timeout == 0.0) {
							remove_client(clients, i, num_clients);
						}
					}
					ReleaseMutex(infoMutex);
				}
				else {
					string msg = string("MOUSE:") + int_to_string(clients[0]->player->mouse_x) + ":" + int_to_string(clients[0]->player->mouse_y) + ":";
					send_datagrams(clients[0]->sock, msg, clients[1]->sai, sizeof(clients[1]->sai));
					WaitForSingleObject(infoMutex, INFINITE);
					clients[1]->timeout = max(clients[1]->timeout - data_time + data_time_old, 0.0);
					if(DEBUG) printf("Timeoutp[%f]\n", clients[1]->timeout);
					if (clients[1]->timeout == 0.0) {
						break;
					}
					ReleaseMutex(infoMutex);
				}
			}
			data_time_old = data_time;
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
			ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
		{
			clients[0]->player->mouse_x = ev.mouse.x;
			clients[0]->player->mouse_y = ev.mouse.y;
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			if (ev.mouse.button & 1) {
				//clients[0]->player->shoot(bullets);
				clients[0]->keys[LMB] = true;
				if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(LMB), clients[1]->sai, sizeof(clients[1]->sai));
			}
		}
		else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
			if (ev.mouse.button & 1) {
				clients[0]->keys[LMB] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(LMB), clients[1]->sai, sizeof(clients[1]->sai));
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch(ev.keyboard.keycode) {
				case ALLEGRO_KEY_UP:
					clients[0]->keys[UP] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(UP), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_DOWN:
					clients[0]->keys[DOWN] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(DOWN), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_LEFT:
					clients[0]->keys[LEFT] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(LEFT), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_RIGHT:
					clients[0]->keys[RIGHT] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(RIGHT), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_W:
					clients[0]->keys[UP] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(UP), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_S:
					clients[0]->keys[DOWN] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(DOWN), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_A:
					clients[0]->keys[LEFT] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(LEFT), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_D:
					clients[0]->keys[RIGHT] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(RIGHT), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_SPACE:
					clients[0]->keys[SPACE] = true;
					if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(SPACE), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				case ALLEGRO_KEY_TAB:
					clients[0]->keys[TAB] = true;
					//if (!server) send_datagrams(clients[0]->sock, "BTNDOWN:" + int_to_string(SPACE), clients[1]->sai, sizeof(clients[1]->sai));
					break;
				default:
					break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				clients[0]->keys[UP] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(UP), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_DOWN:
				clients[0]->keys[DOWN] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(DOWN), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_LEFT:
				clients[0]->keys[LEFT] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(LEFT), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_RIGHT:
				clients[0]->keys[RIGHT] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(RIGHT), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_W:
				clients[0]->keys[UP] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(UP), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_S:
				clients[0]->keys[DOWN] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(DOWN), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_A:
				clients[0]->keys[LEFT] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(LEFT), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_D:
				clients[0]->keys[RIGHT] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(RIGHT), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_SPACE:
				clients[0]->keys[SPACE] = false;
				if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(SPACE), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			case ALLEGRO_KEY_TAB:
				clients[0]->keys[TAB] = false;
				//if (!server) send_datagrams(clients[0]->sock, "BTNUP:" + int_to_string(SPACE), clients[1]->sai, sizeof(clients[1]->sai));
				break;
			default:
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			if (!server) {
				string exit = "EXIT";
				send_datagrams(clients[0]->sock, exit, clients[1]->sai, sizeof(clients[1]->sai));
			}
			break;
		}
		

		if (redraw && al_is_event_queue_empty(queue)) {
			if (!server) WaitForSingleObject(infoMutex, INFINITE);
			redraw = false;

			al_clear_to_color(al_map_rgb(255, 255, 255));

			platforms[0]->draw();
			platforms[1]->draw();
			platforms[2]->draw();
			platforms[3]->draw();
			platforms[4]->draw();

			for (int i = 0; i < num_guns; ++i) guns[i]->draw();

			for (int i = 0; i < num_clients; ++i) {
				clients[i]->draw(font_nick);
			}

			draw_bullets(bullets);

			al_draw_text(font, al_map_rgb(0, 255, 0), 0, 0, ALLEGRO_ALIGN_LEFT, int_to_string(round(current_fps)).c_str());

			if(clients[0]->keys[TAB]) write_scores(clients, num_clients, font, 50, 250, 50);

			al_flip_display();
			time_old = time;
			if (!server) ReleaseMutex(infoMutex);
		}
	}
	al_destroy_display(display);
	al_destroy_font(font);
	al_destroy_timer(timer);
	al_destroy_timer(data_timer);
	al_destroy_event_queue(queue);
	al_destroy_bitmap(shotgun);
	al_destroy_bitmap(ar_texture);
	al_destroy_bitmap(player_bitmap);
	al_destroy_bitmap(platform);
	return 0;
}